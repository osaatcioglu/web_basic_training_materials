function test_class(){
	this.test_property = "test_property";

	this.change_property = function() {
		this.test_property = "changed_property";
	}

	private_method = function(){
		return "private_method";
	}
}

function nested_classes(){
	this.child_class = function(){
		return "child child method";
	}
}

test_array = [1, 2, 3];

test_object = {
	a : 1011,
	b : 2022,
	c : function () {
		return "from function";
	},
	d : new test_class(),
	e : test_array,

};

function print_to_document(str){
	div_element = document.createElement("div");
	div_content = document.createTextNode(str);
  	div_element.appendChild(div_content);
	document.body.appendChild(div_element);
}

function show_examples() {
	print_to_document(test_array[0]);
	print_to_document(test_array[1]);
	print_to_document(test_object['a']);
	print_to_document(test_object['b']);
	print_to_document(test_object.a);
	print_to_document(test_object.c);
	print_to_document(test_object.c());
	print_to_document(test_object.d);
	print_to_document(test_object.d.test_property);
	print_to_document(test_object.d.change_property());
	print_to_document(test_object.d.test_property);
	print_to_document(test_object.e[0]);
	// print_to_document(test_object.d.private_method());
	print_to_document("end");
};